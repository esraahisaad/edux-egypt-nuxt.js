$(function() {
    "use strict";

    function initowlCarousel(element) {
        element.each(function() {
            var config = {
                loop: false,
                nav: $(this).data('nav'),
                dots: $(this).data('dots'),
                items: 1,
                navText: [
                    "<i class='ti-angle-left' aria-hidden='true'></i>",
                    "<i class='ti-angle-right' aria-hidden='true'></i>",
                ]

            };

            var owl = $(this);
            if (owl.data('items')) {
                config.items = owl.data('items');
            }
            if (owl.data('slideSpeed')) {
                config.slideSpeed = owl.data('slideSpeed');
            }
            if (owl.data('margin')) {
                config.margin = owl.data('margin');
            }
            if (owl.data('loop')) {
                config.loop = true;
            }
            if (owl.data('autoplay')) {
                config.autoplay = true;
                config.autoplaySpeed = 1500;
            }
            if (owl.data('center')) {
                config.center = true;
            }
            if (owl.data('nav')) {
                config.nav = owl.data('nav');
            }
            if (owl.data('large')) {
                var desktop = owl.data('large');
            } else {
                var desktop = config.items;
            }
            if (owl.data('smalldesktop')) {
                var smalldesktop = owl.data('smalldesktop');
            } else {
                if (owl.data('large')) {
                    var smalldesktop = owl.data('large');
                } else {
                    var smalldesktop = config.items;
                }
            }
            if (owl.data('medium')) {
                var medium = owl.data('medium');
            } else {
                var medium = config.items;
            }
            if (owl.data('smallmedium')) {
                var smallmedium = owl.data('smallmedium');
            } else {
                var smallmedium = 2;
            }
            if (owl.data('extrasmall')) {
                var extrasmall = owl.data('extrasmall');
            } else {
                var extrasmall = 2;
            }
            if (owl.data('smallest')) {
                var smallest = owl.data('smallest');
            } else {
                var smallest = 1;
            }
            config.responsive = {
                321: {
                    items: smallest,
                    margin: 0
                },
                480: {
                    items: extrasmall,
                },
                769: {
                    items: smallmedium,
                },
                981: {
                    items: medium,
                },
                1200: {
                    items: smalldesktop,
                },
                1501: {
                    items: desktop,
                }
            };
            console.log(config)
            if ($('html').attr('dir') == 'rtl') {
                config.rtl = true;
            }

            owl.owlCarousel(config);

        });
    }

    function checkWidthWindow() {
        if ($(window).width() < 767) {
            if ($('.custom-navbar').length > 0) {
                if ($(window).scrollTop() > $(".custom-navbar").height()) {
                    var header_height = $('.custom-navbar').outerHeight();
                    $('.widget_sidebar_filter_widget').css({ 'top': header_height });
                    $('.widget_sidebar_filter_widget').css({ 'height': 'calc(100vh - ' + header_height + 'px)' });
                } else {
                    $('.widget_sidebar_filter_widget').css({ 'top': 0 });
                    $('.widget_sidebar_filter_widget').css({ 'height': 'calc(100vh - ' + 0 + 'px)' });
                }
            }


        }
    }

    /* fixed header */
    $(document).scroll(function() {
        checkWidthWindow();
        /* var $nav = $(".custom-navbar");
        $nav.toggleClass("scrolled", $(this).scrollTop() > $nav.height());
        var sectionCountries = $(".comparison_school").closest('.page-content');
        var sectionCountriesHeight = sectionCountries.offset().top;
        if ($(this).scrollTop() < sectionCountriesHeight) {
            $(".comparison_school").hide();
            $(".action_toggle_comparison").hide();
        } else {
            $(".comparison_school").show();
            $(".action_toggle_comparison").show();
        } */
    });

    $(window).on('resize', function() {
        checkWidthWindow();
    });

    //open and close tab menu
    $('.nav-tabs-dropdown')
        .on("click", "li a:not('.active')", function(event) {
            console.log("djsads")
            $(this).closest('ul').removeClass("open");
        })
        .on("click", "li a.active", function(event) {
            $(this).closest('ul').toggleClass("open");
        });

    $('.action_toggle_comparison').on("click", function() {
        $('.comparison_school').toggleClass('active');
    });
    if ($('.select2').length > 0) {
        $('.select2').each(function(i, obj) {
            $(obj).select2();
        });
    }

    if ($('.owl-carousel').length > 0) {
        initowlCarousel($('.owl-carousel'))
    }

    checkWidthWindow();

    $('body').on('click', '.mobile-sidebar-btn', function() {
        if ($('.widget_sidebar_filter_widget').length > 0) {
            $('.widget_sidebar_filter_widget').toggleClass('active');
        }
        $('.mobile-sidebar-panel-overlay').toggleClass('active');
    });
    $('body').on('click', '.mobile-sidebar-panel-overlay, .close-sidebar-btn', function() {
        if ($('.widget_sidebar_filter_widget').length > 0) {
            $('.widget_sidebar_filter_widget').removeClass('active');
        }
        $('.mobile-sidebar-panel-overlay').removeClass('active');
    });
    new WOW().init();

});