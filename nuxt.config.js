export default {
    // Global page headers: https://go.nuxtjs.dev/config-head
    head: {
        title: 'edux',
        htmlAttrs: {
            lang: 'en'
        },
        meta: [
            { charset: 'utf-8' },
            { name: 'viewport', content: 'width=device-width, initial-scale=1' },
            { hid: 'description', name: 'description', content: '' },
            { name: 'format-detection', content: 'telephone=no' }
        ],
        link: [
            { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
            {
                rel: "stylesheet",
                href: "/assets/fontawesome/css/all.css"
            },
            {
                rel: "stylesheet",
                href: "/assets/css/themify-icons.css"
            },

            {
                rel: "stylesheet",
                href: "/assets/css/bootstrap.min.css"
            },
            {
                rel: "stylesheet",
                href: "/assets/css/select2.min.css"
            },
            {
                rel: "stylesheet",
                href: "/assets/css/owl.carousel.min.css"
            },
            {
                rel: "stylesheet",
                href: "/assets/css/main.css"
            },
            {
                rel: "stylesheet",
                href: "/assets/css/parent-profile.css"
            },
            {
                rel: "stylesheet",
                href: "/assets/css/school-profile.css"
            },
            {
                rel: "stylesheet",
                href: "/assets/css/portal.css"
            },

        ],
        script: [
            { src: "/assets/js/bootstrap.bundle.min.js", type: "text/javascript" },
            {
                src: "https://code.jquery.com/jquery-3.3.1.slim.min.js",
                type: "text/javascript"
            },
            // {
            //     src = "https://cdn.jsdelivr.net/npm/vee-validate@latest/dist/vee-validate.js",
            //     type: "text/javascript"
            // },
            // {
            //     src: src = "https://unpkg.com/vee-validate@latest",
            //     type: "text/javascript"
            // },
        ]
    },

    // Global CSS: https://go.nuxtjs.dev/config-css
    css: [],


    // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
    plugins: [

        { src: 'plugins/owl.js', ssr: false },
        { src: 'plugins/v-calendar.js', ssr: false },
        { src: 'plugins/slider.js', ssr: false },
        { src: 'plugins/vee-validate.js', ssr: false },
        { src: "~plugins/axiosConfig.js", ssr: true },
        { src: "~plugins/service.js", ssr: true },
        { src: '~/plugins/i18n.js', ssr: true }

    ],
    loading: {
        color: '#15a362',
        height: '4px'
    },
    toast: {
        position: 'top-right',
        duration: 3500,
        keepOnHover: true,
        singleton: true

    },
    router: {

        prefetchLinks: false,
        //linkActiveClass: 'active',
        linkExactActiveClass: 'active',
        // extendRoutes(routes, resolve) {
        //   routes.push({
        //     name: '404',
        //     path: '*',
        //     component: resolve(__dirname, 'pages/404.vue')
        //   })
        // }
    },
    // Auto import components: https://go.nuxtjs.dev/config-components
    components: true,
    components: {
        dirs: [
            '~/components',
            '~/components/global/headerBar',
            '~/components/global/footerApp',
            '~/components/pschools/headerProfileSchool',

        ]
    },

    // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
    buildModules: [],

    // Modules: https://go.nuxtjs.dev/config-modules
    modules: [

        // https://go.nuxtjs.dev/bootstrap
        'bootstrap-vue/nuxt',
        // https://go.nuxtjs.dev/axios
        '@nuxtjs/axios',
        '@nuxtjs/auth',
        '@nuxtjs/i18n',
        // Simple usage
        'vuejs-google-maps/nuxt',
        // Passing options in module configuration
        ['vuejs-google-maps/nuxt', { apiKey: 'xxxxxx', libraries: [ /* rest of libraries */ ] }]
    ],
    // Passing options in module top level configuration
    googleMaps: { apiKey: 'xxxxxx', libraries: [ /* rest of libraries */ ] },




    // Axios module configuration: https://go.nuxtjs.dev/config-axios

    axios: {
        baseURL: process.env.NODE_ENV !== 'production' ? 'https://eduxegypt.esmartpress.com/api/' : 'https://eduxegypt.esmartpress.com/api/',
        credentials: false,
        crossdomain: true,
        proxyHeaders: false,
        params: {
            'lang': 'en'
        }
    },

    // Build Configuration: https://go.nuxtjs.dev/config-build
    build: {
        // Add exception
        transpile: [
            "vee-validate/dist/rules"
        ],

    },

    auth: {
        redirect: {
            login: '/auth/login',
            logout: '/auth/login',
            //callback: '/login',
            home: '/'
        },
        strategies: {
            local: {
                scheme: 'refresh',
                localStorage: {
                    prefix: 'auth.'
                },
                token: {
                    property: 'access_token',
                    maxAge: 1800,
                    global: true,
                    // type: 'Bearer'
                },
                refreshToken: {
                    prefix: 'refresh_token.',
                    property: 'refresh_token',
                    data: 'refresh_token',
                    maxAge: 60 * 60 * 24 * 15
                },

                token: {
                    property: 'api_token',
                    global: true,
                    required: true,
                    type: 'Bearer'
                },
                user: {
                    property: 'user',
                    // autoFetch: true
                },
                endpoints: {
                    login: { url: 'login', method: 'post', propertyName: 'api_token' },
                    refresh: { url: 'token/refresh/', method: 'post' },
                    user: { url: '/profile', method: 'get', propertyName: 'result' },
                    logout: { url: 'login', method: 'delete' }
                },
                // tokenType: ''

            }
        }
    },
    i18n: {
        locales: [{
                code: 'en',
                name: 'English',
                iso: 'en-US',
                file: 'en.json',
                dir: 'ltr',

            },
            {
                code: 'ar',
                name: 'Arabic',
                iso: 'ar-EG',
                file: 'ar.json',
                dir: 'rtl',

            },

        ],
        langDir: 'locales/',
        defaultLocale: 'en',

    }
}