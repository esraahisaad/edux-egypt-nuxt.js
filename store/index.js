export const state = () => {

    return {
        authToken: null,
        isAuthinticated: false,
        // locales: ['en', 'ar'],
        // locale: 'ar',
        followedSchools: []

    };
};
export const getters = {
    authToken(state) {
        return state.authToken;
    },
    isAuthinticated(state) {
        return state.isAuthinticated;
    },
    // isAuthenticated(state) {
    //     return state.auth.loggedIn
    // },

    loggedInUser(state) {
        return state.auth.user
    },
    getFollowedSchools(state) {
        return state.followedSchools
    },
    isFollowedSchool(state, schoolId) {
        console.log("state.followedSchools", state.followedSchools)
        return state.followedSchools.find((school) => {
            return school.id == schoolId
        })
    }
};
export const mutations = {
    // SET_LANG(state, locale) {
    //     if (state.locales.includes(locale)) {
    //         state.locale = locale
    //         localStorage.setItem('locale', locale);
    //     }
    // },
    setAuth(state, token) {
        state.authToken = token;
        state.isAuthinticated = true;
    },
    logOut(state) {
        state.authToken = null;
        state.isAuthinticated = false;
    },
    setFollowedSchools(state, followedSchools) {
        console.log(followedSchools)
        state.followedSchools = followedSchools;
    },

};
export const actions = {
    Set_Auth({ commit }, payload) {
        commit("setAuth", payload);
    },
    Clear_User({ commit }) {
        commit("logOut");
    },
    setFollowedSchoolsAction({ commit }) {
        this.$axios.get('followingSchool').then((res) => {
            commit("setFollowedSchools", res.data.data);
        })
    },
};