// /////////////////////////////////////////////////////////////////////////
// export default function({ $auth, $axios, redirect }, inject) {
//     // Create a custom axios instance
//     const api = $axios.create({
//         headers: {
//             Accept: "application/json",
//             "Content-Type": "multipart/form-data"
//         }
//     });
//     // Set baseURL
//     api.setBaseURL("https://eduxegypt.esmartpress.com/api/");

//     // Set token
//     if ($auth && $auth.$storage && $auth.$storage.getUniversal("loggedIn")) {
//         api.setToken($auth.$storage.getUniversal("user"), "Bearer");
//     }

//     // handle errors
//     api.onError(errors => {
//         $nuxt.$loading.finish();

//         if (errors.response.status == 401) {
//             $nuxt.$toast.fire({
//                 icon: "error",
//                 title: error.response.data.EduXEgypt.msg
//             });
//             redirect("/");
//         } else if (errors.response.status == 403) {
//             $nuxt.$toast.fire({
//                 icon: "error",
//                 title: error.response.data.EduXEgypt.msg
//             });
//             // redirect("/");
//         } else if (errors.response.status == 404) {
//             $nuxt.$toast.fire({
//                 icon: "error",
//                 title: error.response.data.EduXEgypt.msg
//             });
//         } else if (errors.response.status == 422) {
//             Object.values(error.response.data.errors).map(err => {
//                 console.log(err);
//                 $nuxt.$bvToast.toast(err, {
//                     title: "Invalid Data",
//                     autoHideDelay: 10000,
//                     appendToast: true
//                 });
//             });
//         }
//     });

//     api.onRequest(config => {
//         $nuxt.$loading.start();
//     });
//     api.onResponse(response => {
//         $nuxt.$loading.finish();
//     });

//     // Inject to context as $api
//     inject("api", api);
// }